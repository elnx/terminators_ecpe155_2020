/*
 * gpio.h
 *
 *  Created on: Jan 16, 2020
 *      Author: Mason
 */


#ifndef _GPIO_H
#define _GPIO_H

#include <stdint.h>

// Peripheral base addresses.
#define GPIO_PORTB              ((volatile uint32_t *)0x40005000)
#define GPIO_PORTF              ((volatile uint32_t *)0x40025000) // base address

// Peripheral register offsets and special fields
enum {
  GPIO_DIR  =   (0x400 >> 2), // 0 is input 1 is input
  GPIO_PUR  =   (0x510 >> 2), // pull up resistor
  GPIO_DEN  =   (0x51c >> 2), // enable the GPIO
};

enum {
  GPIO_PIN_1 = (1 << 1),
  GPIO_PIN_2 = (1 << 2),
  GPIO_PIN_3 = (1 << 3),
  GPIO_PIN_4 = (1 << 4)
};

#endif /* GPIO_H_ */
