/**********************************************************/
/**  Lab 3 QEI/ Motor Control File ECPE_155 SPRING 2020  **/
/**********************************************************/

// Standard Lib
#include <stdint.h>
#include <stdbool.h>

// System Includes
#include "inc/hw_memmap.h"
#include "inc/tm4c123gh6pm.h"

// Driver Lib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"

// Periph Headers
#include "motor_control.h"

// Global variables
int   position             = 0;
int   total_ticks_traveled = 0;
float rps                  = 0;
float distance_per_tick    = (6 * 3.1415)/128;
float distance_traveled    = 0;
float turn_distance        = (2 * 6 * 3.1415)/4;
float pulse                = 0;

// Quadrature Encoder Interupt Handler
void QEIIntHandler(){
    uint32_t status;
    status = QEIIntStatus(QEI1_BASE, true);

    if((status == QEI_INTTIMER){
        pulse                 = QEIVelocityGet(QEI1_BASE);
        rps                   = (pulse/128);
        position              = QEIPositionGet(QEI1_BASE);
        total_ticks_traveled += position + 1;
        QEIPositionSet(QEI1_BASE, 0);
        QEIIntClear(QEI1_BASE, QEI_INTTIMER);
    }
}

int main(void) {

    // Initialize navigation variables
    float target_distance;
    int   rect_side = 0;
    int   turning   = 0;

    // Rectangle Dimensions
    int x = 40; // rectangle length (mm)
    int y = 40; // rectangle width (mm)

    // Get clock freq
    int freq = SysCtlClockGet();

    // Enable Periphs QEI & GPIO C
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);

    // Config QEI Periph pins
    GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_5 | GPIO_PIN_6);

    //Config QEI Function on gpio pin
    // Pin: PHA1 # 15, QEI Module 1 phase A
    GPIOPinConfigure(GPIO_PC5_PHA1);
    // Pin: PHB1 # 14, QEI Module 1 phase B
    GPIOPinConfigure(GPIO_PC6_PHB1);

    // Configure Motor Control
    m_config();

    // Allow processor to respond to interupts
    IntMasterEnable();

    // Disable and Clear QEI/Interupts
    QEIDisable(QEI1_BASE);
    QEIVelocityDisable(QEI1_BASE);
    QEIIntDisable(QEI1_BASE, QEI_INTTIMER);
    QEIIntClear(QEI1_BASE, QEI_INTTIMER);
    

    // Configure QEI
    QEIConfigure(QEI1_BASE, QEI_CONFIG_CAPTURE_A_B | QEI_CONFIG_SWAP, 127);
    QEIVelocityConfigure(QEI1_BASE, QEI_VELDIV_2, freq);

    QEIPositionSet(QEI1_BASE, 0);
    position = QEIPositionGet(QEI1_BASE);

    // Enable QEI
    QEIEnable(QEI1_BASE);
    QEIVelocityEnable(QEI1_BASE);

    // Enable Interpts
    QEIIntRegister(QEI1_BASE,*QEIIntHandler);
    IntEnable(INT_QEI1);
    QEIIntEnable(QEI1_BASE,QEI_INTTIMER);

    m_forward(0);

    while(1) {
        // Calculate distance traveled based on total ticks
        distance_traveled = (total_ticks_traveled * distance_per_tick);

        // Check if turning or if in x or if in y direction
        // Then calculate percent target distance 
        if (((rect_side % 2) == 0) && (turning == 0)) {
           target_distance = ((x - distance_traveled)/x)*100;
        }
        else if (((rect_side % 2) == 1) && (turning == 0)) {
           target_distance = ((y - distance_traveled)/y)*100;
        }
        else if (turning == 1) {
           target_distance = ((turn_distance - distance_traveled)/turn_distance)*100;
        }

        if (target_distance < 0) {
           target_distance = target_distance * -1;
        }

       // Check if x or y side of rectangle or if robot is turning
       if (((target_distance < 10) || ((distance_traveled > x) && ((rect_side % 2) == 0)) || ((rect_side % 2 == 1) && (distance_traveled > y))) && (turning == 0)){
            if (rps >= 0.4){
               m_speed(35);
            }
            m_stop(100000);

            distance_traveled    = 0;  // reset distance 
            total_ticks_traveled = 0;  // reset ticks
            rect_side           += 1;  // Inrement side
            turning              = 1;  // Update to turn
            QEIPositionSet(QEI1_BASE, 0);
            m_left_turn(0);
       }
       else if ((target_distance < 10 || distance_traveled > turn_distance) && turning == 1){
            if (rps <= 0.4){
               m_speed(50);
            }
            m_stop(100000);

            distance_traveled    = 0;  // reset distance 
            total_ticks_traveled = 0;  // reset ticks
            turning              = 0;  // reset turning
            QEIPositionSet(QEI1_BASE, 0);
            m_forward(0);
       }
       if (rect_side == 5){
           break;
       }

    }
    m_stop(0);

	return 0;
}
