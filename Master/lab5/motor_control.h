void motor_config();
void m_forward(int time);
void m_stop(int time);
void m_reverse(int time);
void m_left(int time);
void m_right(int time);
void m_speed(int duty_cycle);
