function updateSim(timerSim,~,handlesGUI)
% updateSim(timerSim,event,obj,handlesGUI)
% Update the simulation of the robot.  Use current position and movement
% information to plot the next step and update the robot information
%
% Input:
% timerSim - Timer object (required argument for timers)
% event - Structure, contains information about timer call
%   (required argument for timers)
% handlesGUI - Structure, handles to all GUI objects for updating plot and
%               robot info
    
    robotRad = GetRobotConstants(handlesGUI.robotObj); 

    % Get time since last update
    tStep = timerSim.InstantPeriod;
    if isnan(tStep)                  % First function call
        tStep = timerSim.Period;     % Assume period is correct
    end
    
    % Extract values
    state = getRobotState(handlesGUI.robotObj);
    x = state(1);
    y = state(2);
    th = state(3);
    v = state(4:5);
    
    % Check for collisions with walls
    collPts = findCollisions(handlesGUI.robotObj);
    
    % Depending on if walls are hit, update position differently
    if isempty(collPts)         % No walls
        driveNormal(handlesGUI.robotObj,tStep)
    
    elseif size(collPts,1) == 1 % One wall
        if ~collPts(4)          % No corner
            drive1Wall(handlesGUI.robotObj,tStep,collPts)
        else                    % One corner
            driveCorner(handlesGUI.robotObj,tStep,collPts)
        end
        
    else                        % Two walls
        if ~any(collPts(:,4))   % No corners
            drive2Wall(handlesGUI.robotObj,tStep,collPts)
        elseif xor(collPts(1,4),collPts(2,4))   % One corner
            collPts = collPts(find(~collPts(:,4)),:);
            drive1Wall(handlesGUI.robotObj,tStep,collPts)       % Only look at wall
        else                    % Two corners
            % Only look at corner that is closest to the trajectory
            vec1 = [collPts(1,1)-x collPts(1,2)-y]/...
                sqrt((collPts(1,1)-x)^2+(collPts(1,2)-y)^2);
            vec2 = [collPts(2,1)-x collPts(2,2)-y]/...
                sqrt((collPts(2,1)-x)^2+(collPts(2,2)-y)^2);
            [~, closeIdx] = max([dot(v,vec1) dot(v,vec2)]);
            collPts = collPts(closeIdx,:);
            driveCorner(handlesGUI.robotObj,tStep,collPts)
        end
    end
    
    % Extract updated state values
    oldstate = state;
    state = getRobotState(handlesGUI.robotObj);
    x = state(1);
    y = state(2);
    th = state(3);
    
    % Update odometry values
    updateOdom(handlesGUI.robotObj,oldstate,state)
    
    % If in robot-centric view mode move plot focal point
    if get(handlesGUI.robotCentricButton,'Value')
        curr_xlimit = get(handlesGUI.axesMap,'XLim');
        curr_ylimit = get(handlesGUI.axesMap,'YLim');
        half_xdist = (curr_xlimit(2)-curr_xlimit(1))/2;
        half_ydist = (curr_ylimit(2)-curr_ylimit(1))/2;
        set(handlesGUI.axesMap,'XLim',[x-half_xdist x+half_xdist])
        set(handlesGUI.axesMap,'YLim',[y-half_ydist y+half_ydist])
    else
        % Reset to global view
        xVals = str2double(split(get(handlesGUI.xCoords,'String'),','));
        if ((length(xVals) < 2) || (xVals(2) <= xVals(1)))
            set(handlesGUI.axesMap,'XLim',[-5, 5])
        else
            set(handlesGUI.axesMap,'XLim', xVals)
        end
        
        yVals = str2double(split(get(handlesGUI.yCoords,'String'),','));
        if ((length(yVals) < 2) || (yVals(2) <= yVals(1)))
            set(handlesGUI.axesMap,'YLim',[-5, 5])
        else
            set(handlesGUI.axesMap,'YLim', yVals)
        end
    end
    
    % Line approximation of the robot
    circ_numPts = 21;    % Estimate circle as circ_numPts-1 lines
    circ_ang = linspace(0,2*pi,circ_numPts);
    circ_rad = ones(1,circ_numPts)*robotRad;
    [circ_x, circ_y] = pol2cart(circ_ang,circ_rad);
    circ_x = circ_x + x;
    circ_y = circ_y + y;
    
    % Update robot circle and direction line
    set(handlesGUI.robotFig(1),'XData',circ_x)
    set(handlesGUI.robotFig(1),'YData',circ_y)
    set(handlesGUI.robotFig(2),'XData',[x x + 1.5*robotRad*cos(th)])
    set(handlesGUI.robotFig(2),'YData',[y y + 1.5*robotRad*sin(th)])
    
    % Update robot and sensor values displayed
    timeElapsed = round(getTimeElapsed(handlesGUI.robotObj),2);
    timeUpdate = str2double(get(handlesGUI.updateRate, 'String'));
    if (mod(timeElapsed, timeUpdate) == 0)
        robotPoseStr = ['Robot Pose: [', num2str(x,'%0.3f'), ', ', num2str(y,'%0.3f'), ', ', num2str(th,'%0.3f'), ']'];
        set(handlesGUI.robotPoseText, 'String', robotPoseStr);

        [r, f, l] = ReadIRMultiple(handlesGUI.robotObj);
        irSensorStr = ['IR Sensors: [', num2str(r,'%0.3f'), ', ', num2str(f,'%0.3f'), ', ', num2str(l,'%0.3f'), ']'];
        set(handlesGUI.irSensorText, 'String', irSensorStr);
    end
end