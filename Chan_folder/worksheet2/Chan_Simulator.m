function myFirstControlProgram(robotObj)
    % Set constants for this program
    maxDuration = 16;  % Max time to allow the program to run (s)    
    
    % Initialize loop variables
    tStart = tic;        % Time limit marker
        
    %SetFwdVelAngVel(robotObj,0.1,0);
    
    % set the initial position of the robot
    SetRobotStart(robotObj, [0 0 0]);
    
    % Question A
    SetFwdVelAngVel(robotObj,0.4,0.6);
    pause(5);
    % Question B
    SetFwdVelAngVel(robotic, 0.4, 0);
    pause(10);
    
    % Question C
    SetFwdVelAngVel(robotic, 0,2.49);
    SetFwdVelAngVel(robotic, 0, 2);
    pause(1);

    % Enter main loop
    while ((toc(tStart) < maxDuration) && GetRobotRunState(robotObj))

        DistanceSensor(robotObj)
        AngleSensor(robotObj)
        
        % Briefly pause to avoid continuous loop iteration
        pause(0.1)
    end
    
	SetFwdVelAngVel(robotObj,0,0);
    
end