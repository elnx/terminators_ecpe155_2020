function worksheet2(robotObj)
    % Set constants for this program
    maxDuration = 30;  % Max time to allow the program to run (s)    
    
    % Initialize loop variables
    tStart = tic;        % Time limit marker
        
	SetFwdVelAngVel(robotObj,0.1,0);
 
        SetRobotStart(robotObj, [0 0 0])
        
        SetFwdVelAngVel(robotObj, 4, 2/3)
        pause(5)
        SetFwdVelAngVel(robotObj, 0, 2/3)
        pause(5)
        SetFwdVelAngVel(robotObj, 4, 0)
        pause(5)
        SetFwdVelAngVel(robotObj, 0, 8/3)
        pause(5)
        
    % Enter main loop
    while ((toc(tStart) < maxDuration) && GetRobotRunState(robotObj))
        
        DistanceSensor(robotObj)
        AngleSensor(robotObj)
        
        % Briefly pause to avoid continuous loop iteration
        pause(0.1)
    end
    
	SetFwdVelAngVel(robotObj,0,0);
    
end

