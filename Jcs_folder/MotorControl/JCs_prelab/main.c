#include "inc/tm4c123gh6pm.h"
#include <stdint.h>
#include <stdbool.h>
#include "sysctl.h"
#include "gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

#define BASE_F 0x40025000
#define RED     GPIO_PIN_1
#define BLUE    GPIO_PIN_2
#define GREEN   GPIO_PIN_3
#define RGB_Pins RED|BLUE|GREEN
#define INTB    0x00000044
#define INTF    0x000000B8

void intHandler(void);

int main(void)
{
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTB;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTB;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;

    GPIO_PORTB[GPIO_DEN] |= GPIO_PIN_1;
    GPIO_PORTB[GPIO_DIR] |= GPIO_PIN_1;
    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_4;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_4;

    while (true){
        if (GPIO_PORTF[GPIO_PIN_4] | GPIO_PORTB[GPIO_PIN_1] == 0){
            return -1;
        }else if(GPIO_PORTF[GPIO_PIN_4] ^ GPIO_PORTB[GPIO_PIN_1] == 1){
            return 0;
        }else{
            return 1;
        }
    }
}

void intHandler(void){
    if (GPIO_PORTF[GPIO_PIN_4] | GPIO_PORTB[GPIO_PIN_1] != 0){
            GPIO_PORTB[GPIO_PIN_1] = INTB;
            GPIO_PORTF[GPIO_PIN_4] = INTF;

            SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);  //sets 50MHz system clock,uses one of Pll timers,
                                                                                                  // 16MHz exteranal timer and main oscillator
            SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);            //Enables GPIO Port F

            GPIOPinTypeGPIOOutput(BASE_F, RED|BLUE|GREEN); // Sets GPIO port F and RGB pins as outputs


             // generates an infinite loop

            while(1)
            {
                GPIOPinWrite(BASE_F, RGB_Pins, RED); //Turning on the Red LED pin

                SysCtlDelay(1000000); // Delay clock to visibly see next pin

                GPIOPinWrite(BASE_F, RGB_Pins, GREEN); //Turning on the Green LED pin

                SysCtlDelay(1000000); // Delay clock to visibly see next pin

                GPIOPinWrite(BASE_F, RGB_Pins, BLUE);//Turning on the Blue LED pin

                SysCtlDelay(1000000); // Delay clock to visibly see next pin

             }
    }
}
