/********************************************************************/
/**     Lab 4 Bump Sensor Handler C File ECPE_155 SPRING 2020      **/
/********************************************************************/

// Standard Lib
#include <stdint.h>
#include <stdbool.h>

// System Includes
#include "inc/hw_memmap.h"
#include "inc/tm4c123gh6pm.h"

// Driver Lib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"

#include "motor_control.h"

uint32_t status;

void Bump_Handle_Left(){
    status = GPIOIntStatus(GPIO_PORTE_BASE, true);
    GPIOIntClear(GPIO_PORTE_BASE, status);
    int time = 1000000;

    m_reverse(time);
    m_stop(time);
    m_right_turn(time);
    m_stop(time);
}

void Bump_Handle_Right(){
    status = GPIOIntStatus(GPIO_PORTE_BASE, true);
    GPIOIntClear(GPIO_PORTE_BASE, status);
    int time = 1000000;

    m_reverse(time);
    m_stop(time);
    m_left_turn(time);
    m_stop(time);
}

