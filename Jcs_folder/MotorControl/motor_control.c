/***********************************************/
/**  MOTOR_CONTROL File ECPE_155 SPRING 2020  **/
/***********************************************/

// Standard Lib
#include <stdint.h>
#include <stdbool.h>

// Driverlib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"

// system inc
#include "inc/hw_memmap.h"
#include "inc/tm4c123gh6pm.h"

//Pin Definitions
#define PWM_PB6 GPIO_PIN_6
#define PWM_PB7 GPIO_PIN_7
#define LEFT_IN_1 GPIO_PIN_2
#define LEFT_IN_2 GPIO_PIN_3
#define RIGHT_IN_1 GPIO_PIN_6
#define RIGHT_IN_2 GPIO_PIN_7

// Function headers
void m_forward(int time);
void m_stop(int time);
void m_reverse(int time);
void m_left_turn(int time);
void m_right_turn(int time);
void m_speed(int duty_cycle);

// Configure PWM for motor control
void m_config() {

    // Enable PWM periphs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);

    // Configure PWM_GEN_MODE_UP_DOWN
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_UP_DOWN);
    PWMGenConfigure(PWM0_BASE, PWM_GEN_1, PWM_GEN_MODE_UP_DOWN);

    // Config PWM for Pin 6/7 - Port B 
    GPIOPinConfigure(GPIO_PB6_M0PWM0);
    GPIOPinTypePWM(GPIO_PORTB_BASE, PWM_PB6);

    GPIOPinConfigure(GPIO_PB7_M0PWM1);
    GPIOPinTypePWM(GPIO_PORTB_BASE, PWM_PB7);

    // Set Period 
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 1200);
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, 1200);

    // Set Duty Cycle to 50%
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, 600);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 600);

    // Enable PWM generator block timers
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);
    PWMGenEnable(PWM0_BASE, PWM_GEN_1);

    // Enable PWM Outputs
    PWMOutputState(PWM0_BASE, PWM_OUT_0_BIT, true);
    PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, true);

    //Configure GPIO Pins for toggle
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, LEFT_IN_1 | LEFT_IN_2 | RIGHT_IN_1 | RIGHT_IN_2);

	return;
}




// Turn motor forward -> WR/WL: IN1 LOW, WR/WL: IN2 HIGH
void m_forward(int time)
{

    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, ~LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, LEFT_IN_2);
    
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_1, ~RIGHT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_2, RIGHT_IN_2);

    if(time != 0)
    {
        SysCtlDelay(time);
    }
    return;

}

// Turn motor backward -> WR/WL: IN1 HIGH, WR/WL: IN2 LOW
void m_reverse(int time)
{
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, ~LEFT_IN_2);

    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_1, RIGHT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_2, ~RIGHT_IN_2);
        
    if(time != 0)
    {
        SysCtlDelay(time);
    }
    return;
}

// Stop motor -> WR/WL: IN1 HIGH, WR/WL: IN2 HIGH
void m_stop(int time)
{
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, LEFT_IN_2);

    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_1, RIGHT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_2, RIGHT_IN_2);

    if(time != 0)
    {
        SysCtlDelay(time);
    }
    return;
}

// Turn left ->  Move Left wheels backward / Move Right wheels forward
void m_left_turn(int time)
{
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, ~LEFT_IN_2);

    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_1, ~RIGHT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_2, RIGHT_IN_2);

    if(time != 0)
    {
        SysCtlDelay(time);
    }
    return;
}

// Turn right -> Move Left wheels forward / Move right wheels backward
void m_right_turn(int time)
{
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, ~LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, LEFT_IN_2);

    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_1, RIGHT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, RIGHT_IN_2, ~RIGHT_IN_2);

    if(time != 0)
    {
        SysCtlDelay(time);
    }
    return;
}

// Change Motor Speed
void m_speed(int duty_cycle)
{
    float percent_duty = duty_cycle * .01;
    int   pulse_width  = 1200*percent_duty;

    m_stop(0);
    PWMGenDisable(PWM0_BASE, PWM_GEN_0);
    PWMGenDisable(PWM0_BASE, PWM_GEN_1);

    //Set Period
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 1200);
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_1, 1200);

    // Set Duty Cycle to 50%
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, pulse_width);
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, pulse_width);

    //Start timer, enable output
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);
    PWMGenEnable(PWM0_BASE, PWM_GEN_1);

    PWMOutputState(PWM0_BASE, PWM_OUT_0_BIT, true);
    PWMOutputState(PWM0_BASE, PWM_OUT_1_BIT, true);

    return;

}