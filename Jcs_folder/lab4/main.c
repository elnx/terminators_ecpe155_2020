/**********************************************************/
/**     Lab 4 Bump Sensor MAIN File ECPE_155 SPRING 2020      **/
/**********************************************************/

// Standard Lib
#include <stdint.h>
#include <stdbool.h>

// System Includes
#include "inc/hw_memmap.h"
#include "inc/tm4c123gh6pm.h"

// Driver Lib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"

#include "bump_sensor.h"

int main(void) {

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_5);
    GPIOPinTypeGPIOInput(GPIO_PORTE_BASE, GPIO_PIN_4);

    m_config();

    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_5, GPIO_FALLING_EDGE);
    GPIOIntTypeSet(GPIO_PORTE_BASE, GPIO_PIN_4, GPIO_FALLING_EDGE);

    GPIOIntRegister(GPIO_PORTE_BASE, Bump_Handle_Right);
    GPIOIntRegister(GPIO_PORTE_BASE, Bump_Handle_Left);

    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_INT_PIN_5);
    GPIOIntEnable(GPIO_PORTE_BASE, GPIO_INT_PIN_4);



    while(1){
       m_forward(10000);
    }
	
	return 0;
}
