/*********************************************/
/** Lab2 Main PWM File ECPE_155 SPRING 2020 **/
/*********************************************/

// Standard lib
#include <stdint.h>
#include <stdbool.h>

// Driver lib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/pwm.h"

// System Includes
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "inc/tm4c123gh6pm.h"

// Pin Definitions
#define PWM_PB6 GPIO_PIN_6
#define PWM_PB7 GPIO_PIN_7
#define LEFT_IN_1 GPIO_PIN_2
#define LEFT_IN_2 GPIO_PIN_3

// Function Headers
void m_forward();
void m_stop();
void m_reverse();

// main.c 

int main(void) {

    // Enable the PWM0 peripheral/ GPIO pinB
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    
    // Wait for the PWM0 module to be ready.
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_PWM0))
    {
    }

    // Configure the PWM generator for count down mode with immediate updates
    // to the parameters.
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

    //Configure Pin 6 and Pin 7 on Port B for PWM
    GPIOPinConfigure(GPIO_PB6_M0PWM0);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6);

    GPIOPinConfigure(GPIO_PB7_M0PWM1);
    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_7);

    //
    // Set the period. For a 50 KHz frequency, the period = 1/50,000, or 20
    // microseconds. For a 20 MHz clock, this translates to 400 clock ticks.
    // Use this value to set the period.
    //
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 400);
    //
    // Set the pulse width of PWM0 for a 25% duty cycle.
    //
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, 100);
    //
    // Set the pulse width of PWM1 for a 75% duty cycle.
    //
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_1, 300);
    //
    // Start the timers in generator 0.
    //
    PWMGenEnable(PWM0_BASE, PWM_GEN_0);
    //
    // Enable the outputs.
    //
    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT | PWM_OUT_1_BIT), true);

    //Configure GPIO Pins for toggle
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
    GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_3);

    m_forward();

    SysCtlDelay(5000000);
    m_stop();

    SysCtlDelay(5000000);
    m_reverse();

    SysCtlDelay(5000000);
    m_stop();

    while(1)
    {
    }

	return 0;
}




// Turn motor forward (IN1 LOW, IN2 HIGH)
void m_forward()
{

    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, ~LEFT_IN_1);
    GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, LEFT_IN_2);
    return;

}

// Turn motor backward (IN1 HIGH, IN2 LOW)
void m_reverse()
{
        GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, LEFT_IN_1);
        GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, ~LEFT_IN_2);
        return;
}

// Stop motor (IN1 LOW, IN2 LOW)
void m_stop()
{
        GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_1, ~LEFT_IN_1);
        GPIOPinWrite(GPIO_PORTA_BASE, LEFT_IN_2, ~LEFT_IN_2);
        return;
}
