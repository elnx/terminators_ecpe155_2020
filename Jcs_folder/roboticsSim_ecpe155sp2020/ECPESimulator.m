function varargout = ECPESimulator(varargin)
% SIMULATOR MATLAB code for ECPESimulator.fig
%      SIMULATOR, by itself, creates a new SIMULATOR or raises the existing
%      singleton*.
%
%      H = SIMULATOR returns the handle to a new SIMULATOR or the handle to
%      the existing singleton*.
%
%      SIMULATOR('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SIMULATOR.M with the given input arguments.
%
%      SIMULATOR('Property','Value',...) creates a new SIMULATOR or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Simulator_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Simulator_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Simulator

% Last Modified by GUIDE v2.5 25-Jun-2018 13:17:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ECPESimulator_OpeningFcn, ...
                   'gui_OutputFcn',  @ECPESimulator_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before ECPESimulator is made visible.
function ECPESimulator_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ECPESimulator (see VARARGIN)

    % Choose default command line output for ECPESimulator
    handles.output = hObject;

    % ECPE Robot Object
    handles.robotObj = ECPERobot(handles);

    % Get robot dimension constants
    radiusOfRobot = GetRobotConstants(handles.robotObj);

    % Plot robot in default position and store plot handles for updating
    circ_numPts = 21;    % Estimate circle as circ_numPts-1 lines
    circ_ang = linspace(0,2*pi,circ_numPts);
    circ_rad = ones(1,circ_numPts)*radiusOfRobot;
    [circ_x, circ_y] = pol2cart(circ_ang,circ_rad);
    handle_circ = plot(circ_x,circ_y,'b-','LineWidth',1.5);
    hold on;
    handle_line = plot([0 1.5*radiusOfRobot],[0 0],'b-','LineWidth',1.5);
    xlim([-5 5]);
    ylim([-5 5]);
    handles.robotFig = [handle_circ; handle_line];

    % Set up timer to control simulation updates
    timerSim = timer;
    timerSim.BusyMode = 'drop';
    timerSim.ExecutionMode = 'fixedSpacing';
    timerSim.Name = 'RobotSimTimer';    % To specify timer for deletion
    timerSim.ObjectVisibility = 'on';
    timerSim.Period = 0.01;  % Run update of simulation every 0.01 seconds
    timerSim.TasksToExecute = inf; % default value, will perpetually run function
    timerSim.TimerFcn = {@updateSim,handles};
    start(timerSim)

    % Update handles structure
    guidata(hObject, handles);

    set(hObject,'CloseRequestFcn',@robotSimCloseRequestFcn);
    
    % Make sure we see warnings
    warning('on');


% --- Outputs from this function are returned to the command line.
function varargout = ECPESimulator_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;

%---------------------------------------------------------------------
% MENU FUNCTIONS (EAB: not sure what these are used for)

% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    file = uigetfile('*.fig');
    if ~isequal(file, 0)
        open(file);
    end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                         ['Close ' get(handles.figure1,'Name') '...'],...
                         'Yes','No','Yes');
    if strcmp(selection,'No')
        return;
    end

    delete(handles.figure1)

%---------------------------------------------------------------------
% BUTTON UPDATE FUNCTIONS

% --- Executes on button press in loadMapButton.
function loadMapButton_Callback(hObject, eventdata, handles)
% hObject    handle to loadMapButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Import map file
    [filename, pathname, ~]= uigetfile('*.txt','Import Map File');
    if filename                         % Make sure cancel was not pressed
        fid = fopen([pathname filename]);% Get file handle for parsing

        % Parse the file and extract relevant information
        walls = [];
        beacs = [];
        
        % Read all lines
        lineWords = textscan(fid,'%s %f %f %f %f','CommentStyle','%');
        fclose(fid);

        % Check to make sure have four numbers
        if ((length(lineWords{1}) == length(lineWords{2})) && (length(lineWords{1}) == length(lineWords{3})) &&...
                (length(lineWords{1}) == length(lineWords{4})) && (length(lineWords{1}) == length(lineWords{5})))
            for i=1:length(lineWords{1})
                % All good so now check word
                if strcmp(lineWords{1}{i},'wall')
                    walls = [walls ; lineWords{2}(i) ...
                        lineWords{3}(i) lineWords{4}(i) ...
                        lineWords{5}(i)];
                elseif strcmp(lineWords{1}{i},'beacon') 
                    rgb = lineWords{4}(i);
                    beacs = [beacs ; lineWords{2}(i) ...
                        lineWords{3}(i) ...
                        floor(rgb/100) floor(mod(rgb,100)/10) mod(rgb,10) ...
                        lineWords{5}(i)];
                else
                    warning('MATLAB:invalidInput',...
                        'This line in map file %s is unrecognized:\n\t%d',...
                        filename,i)
                end
            end
        else
            warning('MATLAB:invalidInput',...
                'This map file %s is configured incorrectly.\n\t',filename)
        end
        
        % Set map data in robot object
        setMap(handles.robotObj,walls,beacs)

        % Clear old map
        axes(handles.axesMap)
        children = get(gca,'Children');  % All ploted items on axes
        if ~isempty(children)
            delete(children(1:end-2))	% Delete all but robot from axes
        end

        % Plot walls
        for i= 1:size(walls,1)
            plot(walls(i,[1 3]),walls(i,[2 4]),'k-','LineWidth',1)
        end

        % Plot beacons
        for i= 1:size(beacs,1)
            plot(beacs(i,1),beacs(i,2),...
                'Color',beacs(i,3:5),'Marker','o')
            text(beacs(i,1),beacs(i,2),['  b' num2str(beacs(i,6))])
        end

    end

% --- Executes on button press in startProgButton.
function startProgButton_Callback(hObject, ~, handles)
% hObject    handle to startProgButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Choose file to run as autonomous control function
    [fileName, pathName, ~] = ...
        uigetfile('*.m','Load Autonomous Control File');

    if fileName
        fileName = fileName(1:end-2);    % Take off '.m' extension
        if ~strcmp(cd,pathName) % If control program isn't in current directory
            addpath(pathName);   % Add the path to it to allow access
        end	% This will not save over to future sessions of MATLAB unless the user
        % has the setting to save the current search path upon exit

        % Call odometry reading functions to zero readings
        genOdomAng(handles.robotObj);
        genOdomDist(handles.robotObj);
        
        % Update handles structure
        handles.robotObj.robotRun = true;

        % Run autonomous controller function
        feval(fileName,handles.robotObj);
    end

% --- Executes on button press in stopProgramButton.
function stopProgramButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopProgramButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    SetFwdVelAngVel(handles.robotObj,0,0);
    handles.robotObj.robotRun = false;

    
% --- Executes on button press in setStartPosition.
function setStartPosition_Callback(hObject, eventdata, handles)
% hObject    handle to setStartPosition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Set origin data in robot object
    prompt = {'Enter x:', 'Enter y:', 'Enter theta (in radians):'};
    dlg_title = 'Robot Pose';
    num_lines = 1;
    strDataEntered = inputdlg(prompt,dlg_title,num_lines);
    if ~isempty(strDataEntered)
        origin = [str2num(strDataEntered{1}) str2num(strDataEntered{2}) str2num(strDataEntered{3})];

        orgPose = getRobotState(handles.robotObj);
        SetRobotStart(handles.robotObj,origin);
        setState(handles.robotObj,[origin orgPose(4:6)])

        % Check for collisions with walls
        collPts = findCollisions(handles.robotObj);
        if ~isempty(collPts)         % Collision
            warndlg('Start Position collides with wall. Keeping current position.');
            setRobotStart(handles.robotObj,orgPose(1:3));
            setState(handles.robotObj,orgPose);            
        end
    end


% --- Executes on button press in setNoise.
function setNoise_Callback(hObject, eventdata, handles)
% hObject    handle to setNoise (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Import configuration file
    [filename, pathname, ~] = uigetfile('*.txt','Import Configuration File');
    noise = struct;  % Initialize empty structure for noise information
    if filename                         % Make sure cancel was not pressed
        fid = fopen([pathname filename]);% Get file handle for parsing

        % Parse the file and extract relevant information
        lineWords = textscan(fid,'%s %f %f','CommentStyle','%');
        fclose(fid);

        if (length(lineWords{1}) ~= length(lineWords{2})) || (length(lineWords{1}) ~= length(lineWords{3}))
                warning('Cannot read config file %s\n\t',filename)
        else
            % Get information from line
            sensors = {'odometry', 'ir', 'camera'};
            for i=1:length(lineWords{1})
                if any(strcmp(lineWords{1}{i},sensors))
                    % Name the field in the noise structure after the sensor and
                    % store the mean and standard deviation of the noise in it
                    noise.(lineWords{1}{i}) = [lineWords{2}(i), lineWords{3}(i)];
                else
                    warning('MATLAB:invalidInput',...
                    'This line in config file %s is unrecognized:\n\t%d',...
                    filename,i)
                end
            end

            disp('Noise configured:')
            disp(noise)

            % Set noise data in robot object
            setNoise(handles.robotObj,noise)
        end
    end


% --- Executes during object creation, after setting all properties.
function xCoords_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xCoords (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function xCoords_Callback(hObject, eventdata, handles)
% hObject    handle to xCoords (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function yCoords_CreateFcn(hObject, eventdata, handles)
% hObject    handle to yCoords (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

function yCoords_Callback(hObject, eventdata, handles)
% hObject    handle to yCoords (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    

% --- Executes on button press in radio_centric.
function robotCentricButton_Callback(hObject, eventdata, handles)
% hObject    handle to robot_centric (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of robot_centric



function updateRate_Callback(hObject, eventdata, handles)
% hObject    handle to updateRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of updateRate as text
%        str2double(get(hObject,'String')) returns contents of updateRate as a double
    timeUpdate = str2double(get(hObject, 'String'));
    if ((timeUpdate < 0.01) || (mod(timeUpdate,0.01) ~= 0))
        warndlg('Value is not >= 0.01 or multiple of 0.01 - switching to default value.');
        set(hObject, 'String', '0.1');
    end

% --- Executes during object creation, after setting all properties.
function updateRate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to updateRate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on button press in noiseCheckBox.
function noiseCheckBox_Callback(hObject, eventdata, handles)
% hObject    handle to noiseCheckBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of noiseCheckBox
    setNoiseFlag(handles.robotObj, ~get(hObject,'Value'));

%---------------------------------------------------------------------
% OTHER FUNCTIONS
% --- Executes when user attempts to close figure_simulator.
function robotSimCloseRequestFcn(hObject, ~, ~)
% hObject    handle to figure_simulator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Make sure robot doesn't try to run
    handles.robotObj.robotRun = false;

    % Delete the timer to prevent further updates
    timerList= timerfindall('Name','RobotSimTimer');
    if ~isempty(timerList)
        stop(timerList);
        delete(timerList);
    end

    % Close the figure
    delete(hObject);
