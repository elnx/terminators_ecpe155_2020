function move__1st(robotObj)
    % Set constants for this program
    maxDuration = 5;  % Max time to allow the program to run (s)    
    
    % Initialize loop variables
    tStart = tic;        % Time limit marker
        
	SetFwdVelAngVel(robotObj,4,2/3);

    % Enter main loop
    while ((toc(tStart) < maxDuration) && GetRobotRunState(robotObj))

        DistanceSensor(robotObj)
        AngleSensor(robotObj)
        
        % Briefly pause to avoid continuous loop iteration
        pause(0.1)
    end
    
	SetFwdVelAngVel(robotObj,0,0);
    
end