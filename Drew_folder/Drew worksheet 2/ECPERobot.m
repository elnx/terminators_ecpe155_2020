classdef ECPERobot < handle
% Creates definition of class, instances of which will include all
% properties and controls associated with a single ECPERobot robot

    properties(Constant, GetAccess = 'private')
    % Values cannot be changed
    % All units are kg/m/s/rad unless otherwise noted
        radius = 0.08;              % Radius of the robot (model as a circle)
        wheelbase = 0.06;           % Distance between wheels of 255 robot
        rangeIR = 0.5;              % Linear range of the infrared sensor
        rangeMinIR = 0.04;          % Minimum linear range of infrared sensor
        frictionKin= 0.35;          % Coefficient of kinetic friction of robot and wall
        rangeCamera = 4;            % Linear range of the camera for blob detection
        angRangeCamera = pi*60/180; % Angular range of camera (each side)
        cameraDisplace = 0;         % Position of camera along robot's x-axis   
    end
    
    properties(GetAccess = 'private', SetAccess = 'private')
    % Values require get and set methods to view or manipulate
        % Sensor variables
        odomDist;   % Distance traveled since last check from odometry
                    % Format: double
        odomAng;    % Angle turned since last check from odometry
                    % Format: double
        noise;      % Contains noise data for sensors used
                    % Format: structure, fieldnames are sensor names and
                    %   field values are sensor noise [mean standard_dev]
        noiseOnOff; % Flag to ignore or use noise
        
        % Robot state variables
        posAbs;     % Position in absolute coordinates
                    % Format: vector of doubles, [x y]
        velAbs;     % Velocity in absolute coordinates
                    % Format: vector of doubles, [x y]
        thAbs;      % Yaw angle relative to positive x-axis
                    % Format: double (-pi < thAbs <= pi)
        wAbs;       % Angular velocity
                    % Format: double (positive counter-clockwise)
        velInt;     % Forward velocity intended by motor commands
                    % Format: double
        wInt;       % Angular velocity intended by motor commands
                    % Format: double
        
        % Environment variables
        handlesGUI; % Handles to all GUI objects
                    % Format: structure containing handle numbers
                    %   See SimulatorGUI.fig and SimulatorGUI.m for more
                    %   format information on the contents of the structure
        robotStart; % Contains robot start position/orientation information
                    % Format: vector of doubles, [x y th]
        mapWalls;   % Contains obstacle (wall) start and end points
                    % Format: matrix of doubles, columns [x1 y1 x2 y2]
        mapBeacs;   % Containing beacon location, color, and ID information
                    % Format: cell array of doubles, 
                    %   columns {x y red green blue ID}
        startTime;  % Time that started robot
    end
    
    properties(GetAccess = 'public', SetAccess = 'public')
        robotRun; % Variable to determine if running function
                      % Format: bool
    end
    
    methods(Access = 'public')
    % Functions available to call from controller function or other files
    
    %-------------
    % Constructor Function
        function obj = ECPERobot(varargin)
        % obj = ECPERobot
        % Creates instance of the user-defined class ECPE Robot and
        % initializes all properties. Note that if ECPERobot is not
        % called by SimulatorGUI (with handlesGUI argument), full
        % functionality impossible.
        %
        % obj = ECPERobot(handlesGUI)
        % Format of function call from SimulatorGUI. Passes along structure
        % of handles for the GUI to allow full functionality
        %
        % Input:
        % handlesGUI - Structure of handles to GUI objects
        %   e.g. handlesGUI.push_adv - handle for advance button
        %
        % Output:
        % obj - Instance of class ECPERobot with all fields initialized
            
            % Deal with input argument
            % Will be handles to SimulatorGUI if called by simulator
            if ~isempty(varargin) && isstruct(varargin{1})
                obj.handlesGUI = varargin{1};
            else
                obj.handlesGUI = [];
            end
            
            % Assign properties
            obj.robotStart = [0 0 0];         % Default start position at origin
            obj.mapWalls = [];
            obj.mapBeacs = {};
            obj.noise = struct;             % Empty structure to store noise data
            obj.posAbs = obj.robotStart(1:2); % Initial position
            obj.velAbs = [0, 0];             % Assume robot is stationary to start
            obj.thAbs = obj.robotStart(3);
            obj.wAbs = 0;
            obj.velInt = 0;
            obj.wInt = 0;
            obj.odomDist = 0;               % Start odometry at zero
            obj.odomAng = 0;
            obj.startTime = tic;
        end
        
      %-------------  
      % SimulatorGUI and Robot Setup Functions
        function [rad, rIR, wheelbase, rangeMinIR, rangeCamera, angRangeCamera, cameraDisplace] = GetRobotConstants(obj)
        % [rad, rIR, wheelbase, rangeMinIR, rangeCamera, angRangeCamera, cameraDisplace] = getRobotConstants(obj)
        % Output constant properties of the robot for simulator usage
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % rad - Double, radius of robot (m)
        % rIR - Double, range of infrared wall sensor (m)
            
            rad = obj.radius;
            rIR = obj.rangeIR;
            wheelbase = obj.wheelbase;
            rangeMinIR = obj.rangeMinIR;
            rangeCamera = obj.rangeCamera;
            angRangeCamera = obj.angRangeCamera;
            cameraDisplace = obj.cameraDisplace;
        end
        
        function timeElapsed = getTimeElapsed(obj)
        % timeElapsed = getTimeElapsed(obj)
        % Output time passed since robot started
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % timeElapsed - time passed since started
            timeElapsed = toc(obj.startTime);
        end
        
        function robotRuns = GetRobotRunState(obj)
        % robotRuns = GetRobotRunState(obj)
        % Return if robot should be running or stopped
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % robotRuns - boolean, 1 if running, 0 if stopped
            robotRuns = obj.robotRun;
        end
            
        function noiseFlag = getNoiseFlag(obj)
        % noiseFlag = getNoiseFlag(obj)
        % Output the on/off flag for noise
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % noiseFlag - on/off for noise
            noiseFlag = obj.noiseOnOff;
        end
            
       function setNoiseFlag(obj, noiseFlag)
        % setNoiseFlag(obj, noiseFlag)
        % Set the on/off flag for noise
        %
        % Input:
        % obj - Instance of class ECPERobot
        % noiseFlag - on/off for noise
        % 
            obj.noiseOnOff = noiseFlag;
        end

        
        function [start, walls, beacs] = getMap(obj)
        % [start walls lines beacs vwalls] = getMap(obj)
        % Output the obstacle and robot map data for plotting
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % start - Vector containing robot start position information
        % walls - Matrix containing obstacle information
        % lines - Matrix containing line information
        % beacs - Cell array containing beacon information
        % vwalls - Matrix containing virtual wall information
        %   See the properties specification for format information
            
            % Assign variables from object values
            start = obj.robotStart;
            walls = obj.mapWalls;
            beacs = obj.mapBeacs;
        end
        
        function setMap(obj,walls,beacs)
        % setMap(obj,walls,lines,beacs,vwalls)
        % Change the obstacle map data in the robot object
        %
        % Input:
        % obj - Instance of class ECPERobot
        % walls - % Matrix containing obstacle information
        % lines - % Matrix containing line information
        % beacs - % Cell array containing beacon information
        % vwalls - % Matrix containing virtual wall information
        %   See properties specification for format information
            
            % Save map information to object
            obj.mapWalls = walls;
            obj.mapBeacs = beacs;
        end
        
        function success = SetRobotStart(obj,origin)
        % success = SetRobotStart(obj,origin)
        % Change the robotStart data in the robot object. Move the robot to
        % that position.
        %
        % Input:
        % obj - Instance of class ECPERobot
        % origin - Vector of doubles, containing information about the
        %   robot start position in the format [x y th]
        %
        % Output:
        % success - 1 if move robot, -1 if can't
            
            % Get current values
            oldStart = obj.robotStart;
            oldPos = obj.posAbs;
            oldTh = obj.thAbs;

            % Set new position
            obj.robotStart = origin;
            obj.posAbs = origin(1:2);
            obj.thAbs = origin(3);
            
            % Check for collisions with walls
            collPts = findCollisions(obj);

            % If no collisions, return successs 
            if isempty(collPts)
                success = 1;
            else % Collisions, put back at current and return error
                obj.robotStart = oldStart;
                obj.posAbs = oldPos;
                obj.thAbs = oldTh;

                success = -1;
            end
        end
        
        function setNoise(obj,noiseStruct)
        % setNoise(obj,noiseStruct)
        % Change the noise data for the sensors in the robot object
        %
        % Input:
        % obj - Instance of class ECPERobot
        % noise - Structure, containing noise information
        %   Fieldnames are sensor names with values of [mean standard_dev]
        %   eg. noise.ir = [0.003 0.012]
            
            % Set property value
            obj.noise = noiseStruct;
        end
      
      %-------------  
      % Low-level Sensor Functions
        function distIR = genIR(obj)
        % distIR = genIR(obj)
        % Generates a reading for all IR sensors
        %
        % Input:
        % obj - Instance of class CreateRobot
        %
        % Output:
        % distIR - Vector of doubles [right front left],
        %   distance along each line of sight to nearest obstacle
            
            % Get noise parameters
            if (obj.noiseOnOff == 0)
                noiseAvg = 0;
                noiseStDev = 0;
            elseif isfield(obj.noise,'ir')
                noiseAvg = obj.noise.ir(1);
                noiseStDev = obj.noise.ir(2);
            else
                noiseAvg = 0;
                noiseStDev = 0.01;
            end
            
            % Cycle through all sensors
            distIR = obj.rangeIR*ones(1,3);    % Preallocate for speed
            for i = 1:3
                % Calculate position and orientation of the sensor
                % Assume sensors are at the edge of the robot
                if(i == 1) % Right
                    th_sensor = obj.thAbs+(-1)*pi/2;
                elseif(i == 2) % Front
                    th_sensor = obj.thAbs;
                else % Left
                    th_sensor = obj.thAbs+pi/2;
                end
                x_sensor = obj.posAbs(1)+obj.radius*cos(th_sensor);
                y_sensor = obj.posAbs(2)+obj.radius*sin(th_sensor);
                
                % Get noise value to change reading of sonar
                noiseVal = noiseAvg+noiseStDev*randn;
                
                % Solve for distance using general function
                distIR(i) = findDist(obj,x_sensor,y_sensor,...
                    obj.rangeIR+obj.radius,th_sensor);
                
                distIR(i) = distIR(i)-obj.radius; 
                % Do not add sensor noise if sensor saturated to max value
                % Or if it is less than the minimum range
                if (distIR(i)<obj.rangeIR)&&(distIR(i)>obj.rangeMinIR)
                   % Compute bounds on the sensor noise such that noise 
                   % cannot cause sensor saturation in either direction
                   % (max or min).  
                   % NOTE: This results in noise that is not purely Gaussian
                   
                   maxNoiseVal = obj.rangeIR - distIR(i) - 0.0001;
                   minNoiseVal = obj.rangeMinIR - distIR(i) + 0.0001;
                   
                   % Saturate noisey sonar measurement between these values
                   noiseVal = min(max(noiseVal,minNoiseVal),maxNoiseVal);
                   
                   distIR(i) = distIR(i)+noiseVal;
                end
            end
        end
        
        function [ang, dist, color, id] = genCamera(obj)
        % [ang dist color id] = genCamera(obj)
        % Generates the output from the blob detection on the camera,
        % detects only beacons
        % Camera is located at 'cameraDisplace' distance (+0.13m) 
        % along the robot's x-axis.
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % ang - Vector of doubles, each the angle relative to robot at 
        %   which beacon is detected
        % dist - Vector of doubles, each the distance of beacon from camera
        % color - Matrix of doubles of width three (color vector), 
        %   each row the color of beacon detected
        % id  - AR tag ID
            
            % Get robot position and orientation
            x_r = obj.posAbs(1);
            y_r = obj.posAbs(2);
            th_r = obj.thAbs;
            
            % Get camera position and orientation
            x_c = x_r + obj.cameraDisplace*cos(th_r);
            y_c = y_r + obj.cameraDisplace*sin(th_r);
            th_c = th_r;
            
            % Check each beacon against camera ranges
            ang = [];
            dist = [];
            color = [];
            id = [];
            
            for i= 1:size(obj.mapBeacs,1)   % Go through all the beacons
                % Get beacon position
                x_b   = obj.mapBeacs(i,1);
                y_b   = obj.mapBeacs(i,2);
                clr_b = [obj.mapBeacs(i,3),obj.mapBeacs(i,4),obj.mapBeacs(i,5)];
                id_b  = obj.mapBeacs(i,6);
                
                % Find heading and distance from camera to beacon
                angle_rad = atan2(y_b-y_c,x_b-x_c)-th_c;
                ang_b = angle_rad - 2*pi*floor( (angle_rad+pi)/(2*pi) ); %wrapToPi(atan2(y_b-y_c,x_b-x_c)-th_c);
                dist_b = sqrt((x_b-x_c)^2+(y_b-y_c)^2);
                
                % See if there is a wall in the way (blocks field-of-view)
                angle_rad = ang_b+th_c;
                walldist = findDist(obj,x_c,y_c,dist_b, ...
                        (angle_rad - 2*pi*floor( (angle_rad+pi)/(2*pi) ))); %wrapToPi(ang_b+th_c));
                
                % If camera can see beacon, then save beacon information
                if abs(ang_b) <= obj.angRangeCamera && ...
                        dist_b <= obj.rangeCamera && ...
                        walldist == dist_b
                    ang = [ang; ang_b];
                    dist = [dist; dist_b];
                    color = [color; clr_b];
                    id = [id; id_b];
                end
            end
            
            % Add noise
            if ~isempty(ang)
                if (obj.noiseOnOff == 0)
                    noiseAvg = 0;
                    noiseStDev = 0;
                elseif isfield(obj.noise,'camera')
                    noiseAvg = obj.noise.camera(1);
                    noiseStDev = obj.noise.camera(2);
                else
                    noiseAvg = 0;
                    noiseStDev = 0.01;
                end
                ang = ang+noiseAvg+noiseStDev*randn(length(ang),1);
                dist = dist+noiseAvg+noiseStDev*randn(length(ang),1);
            end
        end
        
        function dist = genOdomDist(obj)
        % dist = genOdomDist(obj)
        % Determines how far the robot has traveled since the last call
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % dist - Double, distance traveled since last call from odometry
            
            % Extract property
            % Noise is already added
            dist = obj.odomDist;
                        
            % Reset sensor to record distance since this call
            obj.odomDist = 0;
        end
        
        function ang = genOdomAng(obj)
        % ang = genOdomAng(obj)
        % Determines how far the robot has turned since the last call
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % ang - Double, angle turned since last call from odometry,
        %   positive counter-clockwise
            
            % Extract property
            % Noise is already added
            ang = obj.odomAng;
            
            % Reset sensor to record angle since this call
            obj.odomAng = 0;
        end
              
        function updateOdom(obj,oldstate,newstate)
        % updateOdom(obj,oldstate,newstate)
        % Updates the odometry properties in the robot object
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Input:
        % oldstate - Vector of doubles [x y th v w], values of state from
        %   previous time step
        % newstate - Vector of doubles [x y th v w], values of current state

            % Extract values
            x_o = oldstate(1);
            y_o = oldstate(2);
            th_o = oldstate(3);
            v_o = oldstate(4:5);
            w_o = oldstate(6);
            x_n = newstate(1);
            y_n = newstate(2);
            th_n = newstate(3);

            % Get noise parameters
            if (obj.noiseOnOff == 0)
                noiseAvg = 0;
                noiseStDev = 0;
            elseif isfield(obj.noise,'odometry')
                noiseAvg = obj.noise.odometry(1);
                noiseStDev = obj.noise.odometry(2);
            else
                noiseAvg = 0;
                noiseStDev = 0.02;
            end

            % Distance sensor
            dist = sqrt((x_n-x_o)^2+(y_n-y_o)^2);        % Distance traveled
            dir = sign(dot(v_o,[cos(th_o) sin(th_o)]));  % For/Backwards
            noiseVal = (noiseAvg+noiseStDev*randn)*dist;
            obj.odomDist = obj.odomDist+dir*dist+noiseVal;

            % Angle sensor
            % Assume small turning angles, and account for cases such as 
            % turning from -pi to pi
            turn = min(abs(th_n-th_o),abs(th_n+th_o));
            dir = sign(w_o);
            noiseVal = (noiseAvg+noiseStDev*randn)*turn;
            obj.odomAng = obj.odomAng+dir*turn+noiseVal;
        end
        
        function [x, y, th] = genOverhead(obj)
        % [x y th] = genOverhead(obj)
        % Generate the output of the overhead localization system
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % x - x-coordinate of the robot
        % y - y-coordinate of the robot
        % th - angle of the robot relative to positive x-axis
            
            % Extract values
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
        end
        
      %-------------  
      % Computational Functions
        function dist = findDist(obj,x_sensor,y_sensor,range,th)
        % dist = findDist(obj,x_sensor,y_sensor,range,th)
        % Finds the distance a sensor is measuring at a certain angle
        %
        % Input:
        % obj - Instance of class ECPERobot
        % x_sensor - X-coordinate of the sensor position
        % y_sensor - Y-coordinate of the sensor position
        % range - Linear range of the sensor
        % th - Angle the sensor is investigating in absolute coordinates
        
        % Output:
        % dist - Linear distance along the line of sight of the sensor to
        % 	the closest obstacle
                
            % Create line of sight
            x_range = x_sensor+range*cos(th);   % Range of sensor
            y_range = y_sensor+range*sin(th);
            
            % Find line equation for sensor line
            m_sensor = (y_range-y_sensor)/(x_range-x_sensor);
            if m_sensor > 1e14
                m_sensor = inf;
            elseif abs(m_sensor) < 1e-14
                m_sensor = 0;
            elseif m_sensor < -1e14
                m_sensor = -inf;
            end
            b_sensor = y_sensor-m_sensor*x_sensor;
            
            % Check against every obstacle (individually)
            j = 1;                   % Count variable for intersections
            x_int = [];              % Position of intersections
            y_int = [];
            for i = 1:size(obj.mapWalls,1)% Count variable for obstacles
                % Find line equations for wall lines
                m_wall = (obj.mapWalls(i,4)-obj.mapWalls(i,2))/...
                    (obj.mapWalls(i,3)-obj.mapWalls(i,1));
                if m_wall > 1e14
                    m_wall = inf;
                elseif abs(m_wall) < 1e-14
                    m_wall = 0;
                elseif m_wall < -1e14
                    m_wall = -inf;
                end
                b_wall = obj.mapWalls(i,2)-m_wall*obj.mapWalls(i,1);
                
                % Find intersection of infinitely long walls
                if ~(m_sensor == m_wall)    % Not parallel lines
                    if isinf(m_sensor)      % Vertical sensor line
                        x_hit = x_sensor;
                        y_hit = m_wall*x_hit+b_wall;
                    elseif isinf(m_wall)    % Vertical wall line
                        x_hit = obj.mapWalls(i,1);
                        y_hit = m_sensor*x_hit+b_sensor;
                    else                    % Normal conditions
                        x_hit = (b_wall-b_sensor)/(m_sensor-m_wall);
                        y_hit = m_sensor*x_hit+b_sensor;
                    end
                    
                    % Verify that intersection is on finite lines
                    % Use tolerances to account for rounding errors on
                    % vertical or horizontal lines
                    if x_hit-min(x_sensor,x_range) > -0.001 && ...
                            x_hit-max(x_sensor,x_range) < 0.001 && ...
                            y_hit-min(y_sensor,y_range) > -0.001 && ...
                            y_hit-max(y_sensor,y_range) < 0.001 && ...
                            x_hit-min(obj.mapWalls(i,[1 3])) > -0.001 && ...
                            x_hit-max(obj.mapWalls(i,[1 3])) < 0.001 && ...
                            y_hit-min(obj.mapWalls(i,[2 4])) > -0.001 && ...
                            y_hit-max(obj.mapWalls(i,[2 4])) < 0.001
                        x_int(j) = x_hit;
                        y_int(j) = y_hit;
                        j = j+1;
                    end
                end
            end
            
            % Find closest wall on sensor line
            dist = range;    % Initialize to max range
            if ~isempty(x_int)
                distVec = sqrt((x_int-x_sensor).^2+(y_int-y_sensor).^2);
                dist = min(distVec);  % Find shortest distance to intersections
            end
        end
        
        function collPts = findCollisions(obj)
        % collPts = findCollisions(obj)
        % Check if the robot intersects any walls
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % collPts - Matrix of doubles [x y i f]
        %   x - x-coordinate of closest point to the center of the robot
        %   y - y-coordinate of closest point to the center of the robot
        %   i - Index of wall, used as obj.mapWalls(i,:)
        %   f - Corner flag, is 1 if intersection point is a corner
        %   An empty matrix means no collisions
            
            % Extract variables
            xR = obj.posAbs(1);   % Position of the robot center
            yR = obj.posAbs(2);
            rad = obj.radius;     % Radius of the robot
            
            % Find nearest point on every wall
            collPts = [];
            for i = 1:size(obj.mapWalls,1)
                % Extract wall data
                x1 = obj.mapWalls(i,1);
                y1 = obj.mapWalls(i,2);
                x2 = obj.mapWalls(i,3);
                y2 = obj.mapWalls(i,4);
                
                % Assume wall is infinitely long
                m = (y2-y1)/(x2-x1);        % Slope of wall
                if isinf(m)                 % Vertical wall
                    x0 = x1;
                    y0 = yR;
                elseif m == 0               % Horizontal wall
                    x0 = xR;
                    y0 = y1;
                else                        % Normal conditions of wall
                    b = y1-m*x1;            % Intercept of wall
                    c = yR+xR/m;            % Intercept of perpendicular line through robot
                    
                    % Calculate intersection point of two lines
                    x0 = (c-b)/(m+1/m);
                    y0 = (b-c)/(m^2+1)+c;
                end
                
                % Check if intersection point is not on finite wall
                endPt = 0;
                if x0 > max(x1,x2) || x0 < min(x1,x2) || ...
                        y0 > max(y1,y2) || y0 < min(y1,y2)
                    % Closest point will be nearest endpoint
                    dist1 = sqrt((x1-xR)^2+(y1-yR)^2);
                    dist2 = sqrt((x2-xR)^2+(y2-yR)^2);
                    if dist1 <= dist2
                        x0 = x1;
                        y0 = y1;
                    else
                        x0 = x2;
                        y0 = y2;
                    end
                    endPt = 1;   % Set corner flag
                end
                
                % Check if intersection point is within robot circle
                if sqrt((x0-xR)^2+(y0-yR)^2) <= rad
                    collPts = [collPts ; x0 y0 i endPt];
                end
            end
            
            % Only keep closest two collision points
            if size(collPts,1) > 2
                [~, distIdx] = sort(sqrt((collPts(:,1)-xR).^2+...
                    (collPts(:,2)-yR).^2));
                collPts = collPts(distIdx(1:2),:);
            end
        end
 
      %-------------  
      % State Functions
        function state = getRobotState(obj)
        % state = getState(obj)
        % Extracts current state properties for the simulation program
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % state - Vector of doubles [x y th v w], values of current state
            
            % Extract variables
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
            v = obj.velAbs;
            w = obj.wAbs;
            
            % Put in output format
            state = [x, y, th, v, w];
        end
        
        function setState(obj,state)
        % setState(obj,state)
        % Imports new state properties from the simulation program
        %
        % Input:
        % obj - Instance of class ECPERobot
        % state - Vector of doubles [x y th v w], values of new state
            
            % Update robot object
            obj.posAbs = state(1:2);
            obj.thAbs = state(3);
            obj.velAbs = state(4:5);
            obj.wAbs = state(6);
        end
        
      %-------------  
      % Low-level Movement Functions
        function driveNormal(obj,tStep)
        % driveNormal(obj,tStep)
        % Updates the new position based on the current position and
        % velocities when no walls affect the robot
        %
        % Input:
        % obj - Instance of class ECPERobot
        % tStep - Double, time since the previous state update
            
            % Read current position and velocity values
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
            v = obj.velInt;
            w = obj.wInt;
            
            % Check zero-velocity cases to avoid inf and NaN values
            if w == 0       % Straight path case (use FKM model)
                x_new = x + v*tStep*cos(th);
                y_new = y + v*tStep*sin(th);
                th_new = th;
            elseif v == 0   % Turning only
                x_new = x;
                y_new = y;
                th_new = th + w*tStep;
            else            % Compute position along arc trajectory (ICR model)
                R = v/w;
                x_new = (R*cos(w*tStep)*sin(th) + R*sin(w*tStep)*cos(th) + x - R*sin(th));
				y_new = (R*sin(w*tStep)*sin(th) - R*cos(w*tStep)*cos(th) + y + R*cos(th));
				th_new = th + w*tStep;
            end
            
            % Update linear velocity components
            vx = v*cos(th_new);
            vy = v*sin(th_new);

            % Update position and velocities
            th_new = th_new - 2*pi*floor( (th_new+pi)/(2*pi) );%wrapToPi(th_new); % keep theta in [-pi, pi]
            obj.posAbs = [x_new, y_new];
            obj.thAbs = th_new;
            obj.velAbs = [vx, vy];
            obj.wAbs = w;
        end
        
        function drive1Wall(obj,tStep,collPts)
        % drive1Wall(obj,tStep,collPts)
        % Updates the new position based on the current position and
        % velocities when one wall affects the robot
        %
        % Input:
        % obj - Instance of class ECPERobot
        % tStep - Double, time since the previous state update
        % collPts - Vector of doubles [x y i f]
        %   x - x-coordinate of closest point to the center of the robot
        %   y - y-coordinate of closest point to the center of the robot
        %   i - Index of wall, used as obj.mapWalls(i,:)
        %   f - Corner flag, is 1 if intersection point is a corner
        %   To be used in this function, collPts must have exactly 1 row
            
            % Get important values
            r = obj.radius;
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
            v_int = obj.velInt*[cos(th) sin(th)];
            w_int = obj.wInt;
            muK = obj.frictionKin;
            i_wall = collPts(3);
            
            % Get wall data with x1 <= x2
            [x1,wall_idx] = min(obj.mapWalls(i_wall,[1 3]));
            if wall_idx == 1
                y1 = obj.mapWalls(i_wall,2);
                x2 = obj.mapWalls(i_wall,3);
                y2 = obj.mapWalls(i_wall,4);
            else
                y1 = obj.mapWalls(i_wall,4);
                x2 = obj.mapWalls(i_wall,1);
                y2 = obj.mapWalls(i_wall,2);
            end
            
            % Get tangential vector to the wall in the correct direction
            % That is, counter-clockwise around the robot
            if collPts(2) <= y   % Wall is beneath robot
                tV = [x2-x1 y2-y1]/sqrt((x2-x1)^2+(y2-y1)^2);
            else
                tV = [x1-x2 y1-y2]/sqrt((x1-x2)^2+(y1-y2)^2);
            end
            
            % Get normal vector from wall to robot
            nV = [x-collPts(1) y-collPts(2)]/...
                sqrt((x-collPts(1))^2+(y-collPts(2))^2);
            
            % Put intended velocity into tangential and normal directions
            v_t_int = dot(v_int,tV);
            v_n_int = dot(v_int,nV);
            
            % Compute true normal velocity
            v_n = (v_n_int > 0)*v_n_int; % Make it zero if intended normal 
                                        % velocity is negative (towards wall)
            v_n_int = (v_n_int <= 0)*v_n_int;    % Make zero or negative for
                                                % friction computation
            
            % Check if robot is sliding or is in pure rolling
            
            % Compute angular and tangential velocity
            % Assume normal force ~ normal velocity
            if -w_int*r > v_t_int
                v_t = v_t_int+v_n_int*muK;
                w = w_int+sign(w_int)*v_n_int*muK/r;
            else
                v_t = v_t_int-v_n_int*muK;
                w = w_int-sign(w_int)*v_n_int*muK/r;
            end
            if sign(v_t_int) ~= sign(v_t)   % Motion opposite of intended
                v_t = 0;     % Friction should only resists motion
            end
            if sign(w_int) ~= sign(w)
                w = 0;       % Friction should only resist motion
            end
            
            % Compute cartesian components of velocity
            v_x = v_t*tV(1)+v_n*nV(1);
            v_y = v_t*tV(2)+v_n*nV(2);
            
            % Update position
            obj.posAbs = [x+v_x*tStep  y+v_y*tStep];
            angle_rad = th+w*tStep;
            obj.thAbs = angle_rad - 2*pi*floor( (angle_rad+pi)/(2*pi) );%wrapToPi(th+w*tStep);
            obj.velAbs = [v_x v_y];
            obj.wAbs = w;
        end
        
        function drive2Wall(obj,tStep,collPts)
        % drive2Wall(obj,tStep,collPts)
        % Updates the new position based on the current position and
        % velocities when two walls affect the robot
        %
        % Input:
        % obj - Instance of class ECPERobot
        % tStep - Double, time since the previous state update
        % collPts - Matrix of doubles, columns [x y i f]
        %   x - x-coordinate of closest point to the center of the robot
        %   y - y-coordinate of closest point to the center of the robot
        %   i - Index of wall, used as obj.mapWalls(i,:)
        %   f - Corner flag, is 1 if intersection point is a corner
        %   To be used in this function, collPts must have exactly 2 rows
            
            % Get important values
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
            v_int = obj.velInt*[cos(th) sin(th)];
            w_int = obj.wInt;
            muK = obj.frictionKin;
            i_wall = collPts(:,3);
            
            % Get wall data with x1 <= x2
            x1 = zeros(length(i_wall),1);    % Preallocate for speed
            y1 = zeros(length(i_wall),1);
            x2 = zeros(length(i_wall),1);
            y2 = zeros(length(i_wall),1);
            tV = zeros(length(i_wall),2);
            nV = zeros(length(i_wall),2);
            for j = 1:length(i_wall)
                % Check that wall is not vertical
                if obj.mapWalls(i_wall(j),1) ~= obj.mapWalls(i_wall(j),3)
                    [x1(j), wall_idx] = min(obj.mapWalls(i_wall(j),[1 3]));
                    
                    if wall_idx == 1
                        y1(j) = obj.mapWalls(i_wall(j),2);
                        x2(j) = obj.mapWalls(i_wall(j),3);
                        y2(j) = obj.mapWalls(i_wall(j),4);
                    else
                        y1(j) = obj.mapWalls(i_wall(j),4);
                        x2(j) = obj.mapWalls(i_wall(j),1);
                        y2(j) = obj.mapWalls(i_wall(j),2);
                    end
                    
                    % Get tangential vector to wall in the correct direction
                    % That is, counter-clockwise around the robot
                    if collPts(j,2) < y     % Wall is beneath robot
                        tV(j,:) = [x2(j)-x1(j) y2(j)-y1(j)]/...
                            sqrt((x2(j)-x1(j))^2+(y2(j)-y1(j))^2);
                    else
                        tV(j,:) = [x1(j)-x2(j) y1(j)-y2(j)]/...
                            sqrt((x1(j)-x2(j))^2+(y1(j)-y2(j))^2);
                    end
                else    % Vertical wall
                    [y1(j), wall_idx] = min(obj.mapWalls(i_wall(j),[2 4]));
                    if wall_idx == 1
                        x1(j) = obj.mapWalls(i_wall(j),1);
                        x2(j) = obj.mapWalls(i_wall(j),3);
                        y2(j) = obj.mapWalls(i_wall(j),4);
                    else
                        x1(j) = obj.mapWalls(i_wall(j),3);
                        x2(j) = obj.mapWalls(i_wall(j),1);
                        y2(j) = obj.mapWalls(i_wall(j),2);
                    end
                    
                    % Get tangential vector to wall in the correct direction
                    % That is, counter-clockwise around the robot
                    if collPts(j,1) < x     % Wall is left of robot
                        tV(j,:) = [x1(j)-x2(j) y1(j)-y2(j)]/...
                            sqrt((x1(j)-x2(j))^2+(y1(j)-y2(j))^2);
                    else
                        tV(j,:) = [x2(j)-x1(j) y2(j)-y1(j)]/...
                            sqrt((x2(j)-x1(j))^2+(y2(j)-y1(j))^2);
                    end
                end
                
                % Get normal vector from wall to robot
                nV(j,:) = [x-collPts(j,1) y-collPts(j,2)]/...
                    sqrt((x-collPts(j,1))^2+(y-collPts(j,2))^2);
            end
            
            % Find normal intended velocity components
            v_n1_int = dot(v_int,nV(1,:));
            v_n2_int = dot(v_int,nV(2,:));
            
            % Find if the robot is driving into the corner
            done = false;    % Signal that other sim function was called
            if dot(tV(1,:),tV(2,:)) <= 0    % Walls at acute or right angle
                if (v_n1_int <= 0 && v_n2_int <= 0) || (v_n1_int <= 0 ...
                        && xor(dot(v_int,tV(1,:)) <= 0, ...
                        dot(tV(1,:),nV(1,:)+nV(2,:)) < 0)) || ...
                        (v_n2_int <= 0 && xor(dot(v_int,tV(2,:)) <= 0, ...
                        dot(tV(2,:),nV(1,:)+nV(2,:)) < 0))
                    v = [0 0];   % Stuck in the corner
                    w = w_int+sign(w_int)*muK*(v_n1_int+v_n2_int);
                    if sign(w) ~= sign(w_int)
                        w = 0;   % Friction should only resist motion
                    end
                elseif dot(v_int,nV(1,:)) <= 0
                    collPts = collPts(1,:);  % Drive towards wall 1
                    drive1Wall(obj,tStep,collPts)
                    done = true;
                elseif dot(v_int,nV(2,:)) <= 0
                    collPts = collPts(2,:);  % Drive towards wall 2
                    drive1Wall(obj,tStep,collPts)
                    done = true;
                else                        % Drive away from walls
                    v = v_int;
                    w = w_int;
                end
            else                        % Walls at obtuse
                if v_n1_int > 0 && v_n2_int > 0 % Drive away from walls
                    v = v_int;
                    w = w_int;
                elseif sign(dot(v_int,tV(1,:))) == sign(dot(v_int,tV(2,:)))
                    if v_n1_int < v_n2_int      % Drive towards wall 1
                        collPts = collPts(1,:);
                        drive1Wall(obj,tStep,collPts)
                    else                        % Drive towards wall 2
                        collPts = collPts(2,:);
                        drive1Wall(obj,tStep,collPts)
                    end
                    done = true;
                else                            % Stuck in the corner
                    v = [0 0];   % Stuck in the corner
                    w = w_int+sign(w_int)*muK*(v_n1_int+v_n2_int);
                    if abs(w) > abs(w_int)
                        w = 0;   % Friction should only resist motion
                    end
                end
            end
            
            if ~done    % drive1Wall hasn't been called to update everything
                % Update position
                % Don't use arc calculation to avoid errors
                obj.posAbs = [x+v(1)*tStep  y+v(2)*tStep];
                angle_rad = th+w*tStep;
                obj.thAbs = angle_rad - 2*pi*floor( (angle_rad+pi)/(2*pi) );%wrapToPi(th+w*tStep);
                obj.velAbs = v;
                obj.wAbs = w;
            end
        end
        
        function driveCorner(obj,tStep,collPts)
        % driveCorner(obj,tStep,collPts)
        % Updates the new position based on the current position and
        % velocities when one corner affects the robot
        %
        % Input:
        % obj - Instance of class ECPERobot
        % tStep - Double, time since the previous state update
        % collPts - Matrix of doubles, columns [x y i f]
        %   x - x-coordinate of closest point to the center of the robot
        %   y - y-coordinate of closest point to the center of the robot
        %   i - Index of wall, used as obj.mapWalls(i,:)
        %   f - Corner flag, is 1 if intersection point is a corner
        %   To be used in this function, collPts must have exactly 1 row
            
            % Get important values
            r = obj.radius;
            x = obj.posAbs(1);
            y = obj.posAbs(2);
            th = obj.thAbs;
            v_int = obj.velInt*[cos(th) sin(th)];
            w_int = obj.wInt;
            muK = obj.frictionKin;
            
            % Get normal vector from corner to robot
            nV = [x-collPts(1) y-collPts(2)]/...
                sqrt((x-collPts(1))^2+(y-collPts(2))^2);
            
            if dot(v_int,nV) >= 0   % Moving away from the corner
                driveNormal(obj,tStep)
            else
                % Assume rolling only motion around the corner
                tV = cross([nV 0],[0 0 1]);  % Vector tangential to path
                tV = tV(1:2);                % clockwise around corner
                a = -dot(v_int,tV)*tStep/r;  % Angle to travel
                dnV = (cos(a)-1)*r*nV;   % Movement in normal direction
                dtV = -sin(a)*r*tV;      % Movement in tangential direction
                w = w_int+sign(w_int)*dot(v_int,nV)*muK; % Angular velocity
                if sign(w) ~= sign(w_int) 
                    w = 0;           % Friction should only resist motion
                end
                
                % Update position
                obj.posAbs = [x+dnV(1)+dtV(1)  y+dnV(2)+dtV(2)];
                angle_rad = th-a+w*tStep;
                obj.thAbs = angle_rad - 2*pi*floor( (angle_rad+pi)/(2*pi) );%wrapToPi(th-a+w*tStep);
                obj.velAbs = (dnV+dtV)/tStep;
                obj.wAbs = w;
            end
        end
        
      %-------------  
      % High-Level Measurement Functions
        function AngleR = AngleSensor(obj)
        % AngleR = AngleSensorRobot(obj)
        % Determines how far the robot has turned since the last call
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % AngleR - Double, angle turned since last call from odometry,
        %   positive counter-clockwise (rad)
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to AngleSensor must have class '...
                    'ECPERobot.  Input argument should be the input '...
                    'argument to the control program'])
            else
                % Generate a reading for the odometry and reset it
                AngleR = genOdomAng(obj);
            end
        end

        function Distance = DistanceSensor(obj)
        % Distance = DistanceSensor(obj)
        % Determines how far the robot has traveled since the last call
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % Distance - Double, distance traveled since last call from odometry (m)
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to DistanceSensor must '...
                    'have class ECPERobot.  Input argument should be '...
                    'the input argument to the control program'])
            else
                % Generate a reading for the odometry and reset it
                Distance = genOdomDist(obj);
           end
        end
        
        function distance = ReadIR(obj,irNum)
        % distance = ReadIR(obj,sonarNum)
        % Reads the distance returned by the specified IR sensor
        %
        % Input:
        % obj - Instance of class ECPERobot
        % irNum - Number corresponding to IR to be read
        %   The simulator assumes this ir setup:
        %       1 - Right
        %       2 - Front
        %       3 - Left
        %
        % Output:
        % distance - Double, distance to nearest obstacle in front of 
        %   the robot, or the range of the ir if no obstacle in range
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['The first input argument to ReadIR must have '...
                    'class ECPERobot.  Input argument should be the '...
                    'input argument to the control program'])
            elseif nargin > 2
                error('Simulator:invalidInput',...
                    ['There can be no more than 2 input arguments to '...
                    'ReadIR.'])
            elseif ~isnumeric(irNum) || ~(length(irNum) == 1)
                error('Simulator:invalidInput',...
                    ['The second input argument to ReadIR must be a '...
                    'single number.'])
            elseif irNum < 1 || irNum > 3
                error('Simulator:invalidInput',...
                    ['The second input argument to ReadIR must be a '...
                    'number between 1 and 3.'])
            else
                % Generate reading for sensor
                distIR = genIR(obj);

                % Extract value for output
                distance = distIR(irNum);

                % Check that distance is within limits
                % IR functions output max value otherwise
                if distance <= obj.rangeMinIR || ...
                    distance >= obj.rangeIR
                    distance = obj.rangeIR;
                end  
            end
        end
        
        function [distanceR, distanceF, distanceL] = ReadIRMultiple(obj)
        % distance = ReadIRMultiple(obj,irNum)
        % Reads the distance returned by the IR sensors
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % distanceR - Double, distance to nearest obstacle in the path of 
        %   the right IR sensor
        % distanceF - Double, distance to nearest obstacle in the path of 
        %   the front IR sensor
        % distanceL - Double, distance to nearest obstacle in the path of 
        %   the left IR sensor
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to ReadIRMultiple must have class '...
                    'ECPERobot.  Input argument should be the input '...
                    'argument to the control program'])
            else
                % Generate reading for sensor
                distIR = genIR(obj);

                distanceR = distIR(1);
                distanceF = distIR(2);
                distanceL = distIR(3);

                % Check that distance is within limits
                % IR functions output max value otherwise
                if distanceR < obj.rangeMinIR || ...
                    distanceR > obj.rangeIR
                    distanceR = obj.rangeIR;
                end
                if distanceF < obj.rangeMinIR || ...
                    distanceF > obj.rangeIR
                    distanceF = obj.rangeIR;
                end
                if distanceL < obj.rangeMinIR || ...
                    distanceL > obj.rangeIR
                    distanceL = obj.rangeIR;
                end
            end
        end
        
        function [range, angle, color, Ntag] = ReadBeacon(obj)
        % [range angle color Ntag] = ReadBeacon(obj)
        % Reads the ARtag detection camera and reports polar
        % position in the camera reference frame, as well as the rotation
        % about the tag center
        %
        % Input:
        % obj - Instance of the class ECPERobot
        % 
        % Output: 
        % range     - column vector of ranges
        % angle     - column vector of angles
        % color - column vector of color coordinates in camera frame
        % Ntag  - column vector of AR tag numbers detected in camera frame
        
         % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to ReadBeacon must have class '...
                    'ECPERobot.  Input argument should be the input '...
                    'argument to the control program'])
            else
                % Generate reading for sensor
                [angle, range, color, Ntag] = genCamera(obj);
            end
        end
        
        function [x, y, theta] = ReadOverhead(obj)
        % [x y theta] = ReadOverhead(obj)
        % Read the output of the overhead localization system
        %
        % Input:
        % obj - Instance of class ECPERobot
        %
        % Output:
        % x - x-coordinate of the robot
        % y - y-coordinate of the robot
        % theta - angle of the robot relative to positive x-axis
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to ReadOverhead must have '...
                    'class ECPERobot.  Input argument should be the '...
                    'input argument to the control program'])
            else
                % Generate reading for sensor
                [x, y, theta] = genOverhead(obj);
            end
        end

      %-------------  
      % High-level Drive Functions
        function SetDriveWheels(obj,rightWheel,leftWheel)
        % SetDriveWheels(obj,rightWheel,leftWheel)
        % Controls the forward velocity of each wheel individually
        %
        % Input:
        % obj - Instance of class ECPERobot
        % rightWheel - Double, velocity of right wheel
        % leftWheel - Double, velocity of left wheel
        %   Both values in range [-0.5 0.5] (m/s)
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['The first input argument to SetDriveWheels '...
                    'must have class ECPERobot.  Input argument '...
                    'should be the input argument to the control program'])
            elseif ~isnumeric(rightWheel) || isempty(rightWheel)
                error('Simulator:invalidInput',...
                    ['The second input argument to SetDriveWheels '...
                    'must be numeric in the range [-0.5 0.5]'])
            elseif ~isnumeric(leftWheel) || isempty(leftWheel)
                error('Simulator:invalidInput',...
                    ['The third input argument to SetDriveWheels '...
                    'must be numeric in the range [-0.5 0.5]'])
            else
                % Check for input within range
                %{ 
if rightWheel < -0.5
                    rightWheel = -0.5;
                   disp('Simulator:invalidInput')
                   disp(['Input value rightWheel for function '...
                        'SetDriveWheels too low. Must be in '...
                        'range [-0.5 0.5]. Value set to min.'])
                elseif rightWheel > 0.5
                    rightWheel = 0.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value rightWheel for function '...
                        'SetDriveWheels too high. Must be in '...
                        'range [-0.5 0.5]. Value set to max.'])
                end
                if leftWheel < -0.5
                    leftWheel = -0.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value leftWheel for function '...
                        'SetDriveWheels too low. Must be in '...
                        'range [-0.5 0.5]. Value set to min.'])
                elseif leftWheel > 0.5
                    leftWheel = 0.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value leftWheel for function '...
                        'SetDriveWheels too high. Must be in '...
                        'range [-0.5 0.5]. Value set to max.'])
                end
                %}
                    
                % Change object parameters to new values
                obj.velInt = (rightWheel+leftWheel)/2;  % (vr + vl)/2
                obj.wInt = (rightWheel-leftWheel)/obj.wheelbase;  %(vr - vl)/2l
            end
        end
        
        function SetFwdVelAngVel(obj,FwdVel,AngVel)
        % SetFwdVelAngVel(obj,FwdVel,AngVel)
        % Controls the forward and angular velocity of the robot
        %
        % Input:
        % obj - Instance of class ECPERobot
        % FwdVel - Double, forward velocity in range [-0.5 0.5] (m/s)
        % AngVel - Double, angular velocity in range [-2.5 2.5] (rad/s)
        %   Note that the combination of velocities should not cause each
        %   individual wheel to exceed their limits [-0.5 0.5] (m/s)
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['The first input argument to SetFwdVelAngVel '...
                    'must have class ECPERobot.  Input argument '...
                    'should be the input argument to the control program'])
            elseif ~isnumeric(FwdVel) || isempty(FwdVel)
                error('Simulator:invalidInput',...
                    ['The second input argument to SetFwdVelAngVel '...
                    'must be numeric in the range [-0.5 0.5]'])
            elseif ~isnumeric(AngVel) || isempty(AngVel)
                error('Simulator:invalidInput',...
                    ['The third input argument to SetFwdVelAngVel '...
                    'must be numeric in the range [-2.5 2.5]'])
            else
                % Check for input within range
                %{
                if FwdVel < -0.5
                    FwdVel = -0.5;
                   disp('Simulator:invalidInput')
                   disp(['Input value FwdVel for function '...
                        'SetFwdVelAngVel too low. Must be in '...
                        'range [-0.5 0.5]. Value set to min.'])
                elseif FwdVel > 0.5
                    FwdVel = 0.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value FwdVel for function '...
                        'SetFwdVelAngVel too high. Must be in '...
                        'range [-0.5 0.5]. Value set to max.'])
                end
                
if AngVel < -2.5
                    AngVel = -2.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value AngVel for function '...
                        'SetFwdVelAngVel too low. Must be in '...
                        'range [-0.5 0.5]. Value set to min.'])
                elseif AngVel > 2.5
                    AngVel = 2.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value AngVel for function '...
                        'SetFwdVelAngVel too high. Must be in '...
                        'range [-0.5 0.5]. Value set to max.'])
                end
                %}
                % Limit individual wheel velocities
                wheelRight = FwdVel+AngVel*obj.wheelbase/2;
                wheelLeft = FwdVel-AngVel*obj.wheelbase/2;
                %{
                if abs(wheelRight) > 0.5 || abs(wheelLeft) > 0.5
                    wheelRight = min(max(wheelRight,-0.5),0.5);
                    wheelLeft = min(max(wheelRight,-0.5),0.5);
                    disp(['Warning: desired velocity combination '...
                        'exceeds limits.'])
                    disp('Simulator:invalidInput')
                    disp(['Each wheel can only move at 0.5 m/s. '...
                        'Choose a velocity combination that does '...
                        'not exceed these limits. Values set to max.'])
                    % Recalculate fwd and ang velocities
                    FwdVel = (wheelRight+wheelLeft)/2;
                    AngVel = (wheelRight-wheelLeft)/obj.wheelbase;
                end
                    %}
                % Change object parameters to new values
                obj.velInt  = FwdVel;
                obj.wInt    = AngVel;
            end
        end
        
        function TravelDist(obj,speed,distance)
        % travelDist(obj,speed,distance)
        % Commands robot to move specified distance at specified speed in a
        %   straight line, then stop
        %
        % Input:
        % obj - Instance of class ECPERobot
        % speed - abs(Velocity) within range [0.025 0.5] (m/s)
        % distance - Distance to travel (m)
        %   Direction is controlled by distance (negative distance moves
        %     the robot backwards)
        %   Negative values for speed will be changed to positive
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['The first input argument to TravelDist '...
                    'must have class ECPERobot.  Input argument '...
                    'should be the input argument to the control program'])
            elseif ~isnumeric(speed) || isempty(speed)
                error('Simulator:invalidInput',...
                    ['The second input argument to TravelDist '...
                    'must be numeric in the range [0.025 0.5]'])
            elseif ~isnumeric(distance) || isempty(distance)
                error('Simulator:invalidInput',...
                    ['The third input argument to TravelDist '...
                    'must be numeric'])
            else
                % Check for input within range
                if speed < 0
                    speed = abs(speed);
                    disp('Simulator:invalidInput')
                    disp(['Input value speed for function '...
                        'travelDist should not be negative. Sign '...
                        'is changed'])
                elseif speed < 0.025
                    speed = 0.025;
                    disp('Simulator:invalidInput')
                    disp(['Input value speed for function '...
                        'travelDist too low. Value set to min.'])
                end
                
                if speed > 0.5
                    speed = 0.5;
                    disp('Simulator:invalidInput')
                    disp(['Input value speed for function '...
                        'travelDist too high. Value set to max.'])
                end
                    
                % Change object parameters to new values
                obj.wInt = 0;
                obj.velInt = sign(distance)*speed;

                % Use odometry to travel to end point
                olddist = obj.odomDist;
                while abs(obj.odomDist-olddist) < abs(distance)
                    pause(0.05);
                end
                obj.velInt = 0;     % Stop when done
                disp('Done travelDist')
            end
        end
        
        function TurnAngle(obj,speed,angle)
        % turnAngle(obj,speed,angle)
        % Commands robot to turn specified angle at a speed, then stop
        %
        % Input:
        % obj - Instance of class ECPERobot
        % speed - abs(Angular Velocity) within range [0.025 0.2] (rad/s)
        % angle - Angle to turn within range [-pi pi] (rad)
        %   Direction is controlled by angle based on shortest path
        %     angle in [0 180] or [-180 -360] turns counter-clockwise
        %     angle in [180 360] or [0 -180] turns clockwise
        %   Negative values for speed will be changed to positive
            
            % Check for valid input
            if ~isa(obj,'ECPERobot')
                error('Simulator:invalidInput',...
                    ['Input to TurnAngle must have class ECPERobot.'...
                    '  Input argument should be the input argument '...
                    'to the control program'])
            elseif ~isnumeric(speed) || isempty(speed)
                error('Simulator:invalidInput',...
                    ['The second input argument to TravelDist '...
                    'must be numeric in the range [0.025 0.5]'])
            elseif ~isnumeric(angle) || isempty(angle)
                error('Simulator:invalidInput',...
                    ['The third input argument to TravelDist '...
                    'must be numeric'])
            else
                % Check for input within range
                if speed < 0
                    speed = abs(speed);
                    disp('Simulator:invalidInput')
                    disp(['Input value speed for function '...
                        'TurnAngle should not be negative. Sign '...
                        'is changed.'])
                elseif speed < 0.025
                    speed = 0.025;
                    disp(['Input value speed for function '...
                        'TurnAngle too low. Value set to min.'])
                end
                
                if speed > 0.2
                    speed = 0.2;
                    disp('Simulator:invalidInput')
                    disp(['Input value speed for function '...
                        'TurnAngle too high. Value set to max.'])
                end
                
                if angle < -pi
                    disp('Setting angle to be between +/- pi radians')
                    disp('Simulator:invalidInput')
                    disp(['Input value angle for function '...
                        'TurnAngle too low. Value set to equivalent '...
                        'angle in range [-pi pi].'])
                elseif angle > pi
                    disp('Setting angle to be between +/- pi radians')
                    disp('Simulator:invalidInput')
                    disp(['Input value angle for function '...
                        'TurnAngle too high. Value set to equivalent '...
                        'angle in range [-pi pi].'])
                end
                angle = angle - 2*pi*floor( (angle+pi)/(2*pi) ); %wrapToPi(angle);
                    
                % Choose correct turning direction for shortest path
                if angle > 0
                    disp(['Setting turn path to shortest route. '...
                        'Going to turn counter-clockwise'])
                else
                    disp(['Setting turn path to shortest route. '...
                        'Going to turn clockwise'])
                    speed = -speed;
                end
                    
                % Change object parameters to new values
                obj.velInt = 0;
                obj.wInt = speed;
                    
                % Use odometry to travel to end point
                oldang = obj.odomAng;
                while abs(obj.odomAng-oldang) < abs(angle)
                    pause(0.05);
                end
                obj.wInt = 0;   % Stop when done
                disp('Done TurnAngle')
            end
        end
        
     end
end