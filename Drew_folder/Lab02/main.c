#include "inc/tm4c123gh6pm.h"
#include <stdint.h>
#include <stdbool.h>
#include "sysctl.h"
#include "gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"


int main (){

    // SysCtlClockSet(SYSCTL_SYSDIV_2_5|SYSCTL_USE_PLL|SYSCTL_OSC_MAIN|SYSCTL_XTAL_16MHZ);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);           //Enables GPIO Port B
    // Enable PMW0 and PMW1 Peripheral
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);

    GPIOPinTypePWM(GPIO_PORTB_BASE, GPIO_PIN_6);

    GPIOPinConfigure(GPIO_PB6_M0PWM0);


    // Configure the PWM generator for count down mode with immediate updates
    // to the parameters.
    PWMGenConfigure(PWM0_BASE, PWM_GEN_0, PWM_GEN_MODE_DOWN | PWM_GEN_MODE_NO_SYNC);

    // Set the period. For a 50 KHz frequency, the period = 1/50,000, or 20
    // microseconds. For a 20 MHz clock, this translates to 400 clock ticks.
    // Use this value to set the period
    PWMGenPeriodSet(PWM0_BASE, PWM_GEN_0, 400);

    // Set to 75% duty cycle
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_0, 300);

    PWMGenEnable(PWM0_BASE, PWM_GEN_0);

    PWMOutputState(PWM0_BASE, (PWM_OUT_0_BIT ), true);

    while(1){

    }

}
