#include "inc/tm4c123gh6pm.h"
#include <stdint.h>
#include <stdbool.h>
#include "sysctl.h"
#include "gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "math.h"

#define INTD 0x0000004C
#define FALSE 0
#define TRUE 1
char bumper = FALSE;

int main1(){

    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTD;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTD;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF;

    GPIO_PORTD[GPIO_DEN] |= GPIO_PIN_1;
    GPIO_PORTD[GPIO_DIR] &= ~GPIO_PIN_1;

    GPIO_PORTD[GPIO_DEN] |= GPIO_PIN_2;
    GPIO_PORTD[GPIO_DIR] &= ~GPIO_PIN_2;

    GPIO_PORTD[GPIO_DEN] |= GPIO_PIN_3;
    GPIO_PORTD[GPIO_DIR] &= ~GPIO_PIN_3;

    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_1;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_1;

    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_2;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_2;

    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_3;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_3;

    while (TRUE){
        int i;
        if (GPIO_PORTD[GPIO_PIN_1] == GPIO_PIN_1){
            bumper = TRUE;
        }else if (GPIO_PORTD[GPIO_PIN_2] == GPIO_PIN_2){
            bumper = TRUE;
        }else if (GPIO_PORTD[GPIO_PIN_3] == GPIO_PIN_3){
            bumper = TRUE;
        }
        if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_1]==GPIO_PIN_1)){
           for(i = 0; i < 1000000; ++i ){
           GPIO_PORTF[GPIO_PIN_1] ^= GPIO_PIN_1;  // Toggle the "red" LED
           GPIO_PORTD[INTD];
           }
        }else if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_2]==GPIO_PIN_2)){
            for(i = 0; i < 1000000; ++i ){
            GPIO_PORTF[GPIO_PIN_2] ^= GPIO_PIN_2;  // Toggle the "blue" LED
            GPIO_PORTD[INTD];
            }
        }else if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_3]==GPIO_PIN_3)){
            for(i = 0; i < 1000000; ++i ){
            GPIO_PORTF[GPIO_PIN_3] ^= GPIO_PIN_3;  // Toggle the "green" LED
            GPIO_PORTD[INTD];
            }
        }
    }
}
