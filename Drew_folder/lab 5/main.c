#include "inc/tm4c123gh6pm.h"
#include <stdint.h>
#include <stdbool.h>
#include "sysctl.h"
#include "gpio.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "math.h"
#include "motor_control.c"
#include "motor_control.h"
#include "bumper.c"

int v0;

void main(void)
{
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTC;
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTC;
    GPIO_PORTC[GPIO_DEN] |= GPIO_PIN_4;
    GPIO_PORTC[GPIO_DIR] &= ~GPIO_PIN_4;

    GPIO_PORTC[GPIO_PIN_4] = v0;

	while (true){
	    switch (v0){
	    case '0':
	            m_forward(1000000);
	    case '1':
	            m_stop(2);
	            m_reverse(4);
	            m_left(6);
	    }if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_1]==GPIO_PIN_1)){         //Front left bumper
	      m_reverse(4);
	      m_right(4);
	    }else if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_2]==GPIO_PIN_2)){        //Front right bumper
	      m_reverse(4);
	      m_left(4);
	    }else if ((bumper == TRUE) && (GPIO_PORTD[GPIO_PIN_3]==GPIO_PIN_3)){        //Back bumper
	      m_left(4);
	      m_forward(1000000);
	    }
	}
}
