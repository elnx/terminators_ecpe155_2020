#include "inc/tm4c123gh6pm.h"
#include <stdint.h>
#include <stdbool.h>
#include "sysctl.h"
#include "gpio.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

#define BASE_F 0x40025000

#define RED     GPIO_PIN_1
#define BLUE    GPIO_PIN_2
#define GREEN   GPIO_PIN_3
#define RGB_Pins RED|BLUE|GREEN

void main(void)
{

    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF; // Turn on clock gate control
    SYSCTL[SYSCTL_RCGCGPIO] |= SYSCTL_RCGCGPIO_PORTF; // Need two to ensure the clock gate control turns on

    // Enabling the 3 LED gpio Pins
    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_1;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_1;

    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_2;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_2;

    GPIO_PORTF[GPIO_DEN] |= GPIO_PIN_3;
    GPIO_PORTF[GPIO_DIR] |= GPIO_PIN_3;

    while(true){
        uint32_t i;
        for(i = 0; i < 1000000; ++i );
        // Toggle the "red" LED
        GPIO_PORTF[GPIO_PIN_1] ^= GPIO_PIN_1;
        for(i = 0; i < 1000000; ++i );
        // Toggle the "blue" LED
        GPIO_PORTF[GPIO_PIN_2] ^= GPIO_PIN_2;
        for(i = 0; i < 1000000; ++i );
        // Toggle the "green" LED
        GPIO_PORTF[GPIO_PIN_3] ^= GPIO_PIN_3;
        }

}


/**int main (void){

    SysCtlClockSet(SYSCTL_SYSDIV_4|SYSCTL_USE_PLL|SYSCTL_XTAL_16MHZ|SYSCTL_OSC_MAIN);  //sets 50MHz system clock,uses one of Pll timers,
                                                                                          // 16MHz exteranal timer and main oscillator
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);            //Enables GPIO Port F

    GPIOPinTypeGPIOOutput(BASE_F, RED|BLUE|GREEN); // Sets GPIO port F and RGB pins as outputs


     // generates an infinite loop

    while(1)
    {
        GPIOPinWrite(BASE_F, RGB_Pins, RED); //Turning on the Red LED pin

        SysCtlDelay(1000000); // Delay clock to visibly see next pin

        GPIOPinWrite(BASE_F, RGB_Pins, GREEN); //Turning on the Green LED pin

        SysCtlDelay(1000000); // Delay clock to visibly see next pin

        GPIOPinWrite(BASE_F, RGB_Pins, BLUE);//Turning on the Blue LED pin

        SysCtlDelay(1000000); // Delay clock to visibly see next pin

     }



}
**/
