/*
 * sysctl.h
 *
 *  Created on: Jan 16, 2020
 *      Author: Mason
 */

#ifndef SYSCTL_H_
#define SYSCTL_H_

#include <stdint.h>

// Peripheral base addresses.
#define SYSCTL          (((volatile uint32_t *)0x400fe000))

// Peripheral register offsets and special fields.
enum {
  SYSCTL_RCGCGPIO =       (0x608 >> 2), // shift address by 608 to allow the ports on
  #define   SYSCTL_RCGCGPIO_PORTB (1 << 1)
  #define   SYSCTL_RCGCGPIO_PORTD (1 << 3)
  #define   SYSCTL_RCGCGPIO_PORTF (1 << 5) // port F is shifted 5
};

#endif /* SYSCTL_H_ */
